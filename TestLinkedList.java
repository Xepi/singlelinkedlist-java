import java.util.Scanner;

public class TestLinkedList
{
	public static void main(String[]args)
	{
		SingelLinkedList  myList = new SingelLinkedList();
		Scanner input = new Scanner(System.in);

		System.out.println("Welcome!");
		System.out.println("This program will prosess a single linked list \nand different modifications will be made!");
		System.out.println("What would you like to do?");
		System.out.println("1: Delete the first element in the list");
		System.out.println("2: Delete the last element in the list");
		System.out.println("3: Delete the element with a value from the list");
		System.out.println("4: Add an element with a value in the start of the list");
		System.out.println("5: Add an element with a value in the back of the list");
		System.out.println("6: Add an element after an element with a value");
		System.out.println("7: Add en element in front of an element with a value");
		System.out.println("8: Print out the length of the list");
		System.out.println("9: Count numbers of occurences of element with a value in the list,\nthis number will be printed out");
		System.out.println("10: Print out the whole list. Max five elements per line");
		System.out.println("11: Delete the whole list. The number of elements that was deleted,\nwill be printed out.");
		System.out.println("0: Close the program!");
		System.out.println("\nWhat would you like to do?");

		int i = 1;
		while(i == 1)
		{

		int value,todo, element;
		todo = input.nextInt();

			if(todo == 1)
			{
				myList.deleteFirst();
				System.out.println("\n" + myList);
			}

			else if(todo == 2)
			{
				myList.deleteBack();
				System.out.println("\n" + myList);
			}

			else if(todo == 3)
			{
				System.out.println("What element do you want to delete?");
				element = input.nextInt();
				myList.deleteElement(element);
				System.out.println("\n" + myList);
			}

			else if(todo == 4)
			{
				System.out.println("Which value do you want to add?");
				value = input.nextInt();
				myList.insertInFront(new Node(value, null));
				System.out.println("\n" + myList);
			}

			else if(todo == 5)
			{
				System.out.println("Which value do you want to add?");
				value = input.nextInt();
				myList.insertToBack(new Node(value, null));
				System.out.println("\n" + myList);

			}

			else if(todo == 6)
			{
				System.out.println("Which value do you want to add?");
				value = input.nextInt();
				System.out.println("After which element?");
				element = input.nextInt();
				myList.insertAfterElement(new Node(value, null), element);
				System.out.println("\n" + myList);
			}

			else if(todo == 7)
			{
			System.out.println("Which value do you want to add?");
			value = input.nextInt();
			System.out.println("In front of which element?");
			element = input.nextInt();
			myList.insertInFrontOfElement(new Node(value, null), element);
			System.out.println("\n" + myList);
			}

			else if(todo == 8)
			{
				System.out.println(myList.getElements());
			}

			else if(todo == 9)
			{
				System.out.println("Which element do u want to count up?");
				element = input.nextInt();
				int result = myList.howManyOccurences(element);
				System.out.println(result);
			}

			else if(todo == 10)
			{
				System.out.println("\n" + myList);
			}

			else if(todo == 11)
			{
				myList.deleteList();
			}
			else if(todo == 0)
			{
				i = 0;
				break;
			}
			System.out.println("What would you like to do next?");
		}
	}
}