class SingelLinkedList
{
	private Node head;
	private int elements;

    public SingelLinkedList()
    {
		head = null;
		elements = 0;
	}

	public void deleteFirst()
	{
		Node current = head;
		if(current == null)
		{
			System.out.println("List is empty");
		}
		else if(elements == 1)
		{
			head = null;
			elements--;
		}
		else
		{
			head = head.next;
			elements--;
		}
	}

	public void deleteBack()
	{
		Node current = head;
		if(head == null)//listen er tom
		{
			System.out.println("List is empty");
		}
		else if(elements == 1)//hvis det er et element, s� gj�r dette
		{
			head = null;
			elements--;
		}
		else
		{
			while(current.next.next != null)//finner elementet og holder seg en node foran
			{
				current = current.next;
			}
			current.next = null;
			elements--;
		}
	}

	public void deleteElement(int element)
	{
		Node current = head;
		Boolean NoNode = false; //en boolean variabel som hjelper meg med � skille handlinger fra hverandre
		if(head == null) //hvis lista er tom!
		{
			System.out.println("List is empty");
		}
		else if(head.element == element)//hvis elementet ligger f�rst i lista
		{
			head = head.next;
		}
		else//hvis elementet ligger hvor som helst
		{
			while(current.next.element != element)//pr�ver � finne elementet men v�re en mode foran der elementet ligger
			{
				current = current.next;
				if(current.next == null)//finner ikke elementet i en node
				{
					NoNode = true;
					break; //hopper ut av l�kka
				}
			}

			if(NoNode == false)//fant elementet og slettet det
			{
				current.next = current.next.next;
				elements--;
			}
			else if(NoNode == true)
				System.out.println("Couldn't find the element");
		}

	}

	public void insertInFront(Node x) //legger til node fremst
	{
		x.next = head;
		head = x;
		elements++;
	}

	public void insertToBack(Node x) //legger til node bakerst
	{
		Node current = head;
		if(current == null)//hvis det ikke er noen noder i lista
			insertInFront(x);//bruk insertInFront metode
		else if(current.next == null)//Hvis det er 1 node i lista settes den bakerst (ble brukt i metode insertAfterElement n�r det var 1 node
		{
			current.next = x;
			elements++;
		}
		else//ellers s� legges den til bakerst
		{
			while(current.next != null)//finner den siste noden
				current  = current.next;

			current.next = x;
			elements++;
		}
	}

	public void insertAfterElement(Node x, int element)
	{
		Node current = head;
		int NoNode = 0;
		if(head == null) //Hvis det ikke er noen noder i lista
		{System.out.println("List is empty");}
		else if(head.next == null)//Hvis det er 1 node i lista
		{
			if(current.element == element)//skjekkes det om elementet ligger i den noden, hvois den gj�r det s� bruker jeg insertToBack()
			{
				NoNode = 2;
				insertToBack(x);
			}
			else if(current.element != element)//hvis ikke s� fant han ikke noden og hoppet ut av if'en
			{
				NoNode = 1;
			}
		}
		else
		{
			while(current.element != element) //en while som leter etter elementet
			{
				current = current.next; //g�r til neste node
				if(current.next == null) //hvis man er p� siste noden
				{
					if(current.element == element)//ligger elementet i siste noden skjer dette
					{
						current.next = x;
						elements++;
						NoNode = 2;
						break;
					}
					else //Finner han ikke elementet s� breaker han ut av l�kka
					{
						NoNode = 1;
						break;
					}
				}


			}
		}
		if(NoNode == 0) //elementet er funnet, og plasserer det p� plass
		{
			x.next = current.next;
			current.next = x;
			elements++;
		}
		else if(NoNode == 1) //finner ikke noden
			System.out.println("Couldn't find the element");
		else if(NoNode == 2) {}
	}

	public void insertInFrontOfElement(Node x, int element)
	{
		Node current = head;
		Boolean NoNode = false;
		if(current.element == element)//Hvis elementet er fremst i lista
		{
			insertInFront(x); //bruk denne metoden
		}
		else
		{
			while(current.next.element != element)//Hopper  av while hvis element er i neste node
			{
				current = current.next;//g�r igjennom alle nodene
				if(current.next == null)//hvis den er
				{
					NoNode = true;
					break;
				}

			}
			if(NoNode == false)
			{
				x.next = current.next;
				current.next = x;
				elements++;
			}
		}
	}

	public int getElements()
	{
		return elements;
	}

	public int howManyOccurences(int element)
	{
		Node current = head;
		int occurences = 0;
		if(head == null)
		{
			occurences = 0;
		}
		else
		{
			while(current.next != null)
			{
				if(current.element == element)
					occurences++;
				current = current.next;
			}
		}
		return occurences;
	}

	public String toString()
	{
		String print = "";
		Node current = head;
		int lines = 0;
		while (current != null)
		{
			print = print + current.element;
			current = current.next;
			lines++;
			if(lines == 5)
			{
				print = print + "\n\n";
				lines = 0;
			}
			else
				print = print + " ";
		}

		return "This is your current list:\n" + print;
	}

	public void deleteList()
	{
		head = null;
		System.out.println(elements);
		elements = 0;
	}


}