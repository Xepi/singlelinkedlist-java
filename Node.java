public class Node
{
	int element;
	Node next;

	public Node(int e, Node x)
	{
		element = e;
		next = x;
	}
}